package main

import (
	"fmt"
)

func main() {
	s1 := make([]int, 2)
	fmt.Println(s1)
	s2 := make([]int, 3)
	fmt.Println(s2)

	s4 := []int{}
	s4 = append(s2, 3, 4)
	fmt.Println(s4)
}
