package main

import (
	"fmt"
)

func Pointer() {
	x, y := 10, 20

	p := &x
	fmt.Println(p)
	fmt.Println("---------------------------------------------------------")
	fmt.Println(*p)
	q := &y
	fmt.Println(*q)
}

func main() {

	Pointer()
}
