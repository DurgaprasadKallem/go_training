package main

import (
	"fmt"
)

func main() {

	fmt.Println(Student{firstname: "Durga prasad", lastname: "kallem", phoneNumber: 12345})
	fmt.Println(Student{firstname: "bunty", phoneNumber: 324455})
	fmt.Println(&Student{firstname: "malli", lastname: "boy"})
	fmt.Println(newStudent("srikar", "sins"))

}

type Student struct {
	firstname   string
	lastname    string
	phoneNumber int
}

func newStudent(firstname, lastname string) *Student {
	s := Student{firstname: firstname, lastname: lastname}
	s.phoneNumber = 1249756
	return &s
}
